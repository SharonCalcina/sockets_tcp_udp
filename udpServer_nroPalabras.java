package UDP;

import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.StringTokenizer;

public class udpServer_nroPalabras {
	static private DatagramSocket UDP;
	
public static void main(String[] args) throws IOException {
	
		try {
			UDP = new DatagramSocket(8888);
			System.out.println(" --------- SERVIDOR INICIALIZADO ---------");
			System.out.println("------ En espera de solicitudes ------");
			
			while(true){
				DatagramPacket received = new DatagramPacket(new byte[1024],1024);
				UDP.receive(received);
				String cadena= new String(received.getData());
				System.out.println("cadena: " +cadena.trim());
				String p="su cadena tiene un numero de palabras igual a "+nroPal(cadena)+"";
				byte mensajeEnviar[] = new byte[1024];
				mensajeEnviar = p.getBytes();
				DatagramPacket paqueteAEnviar = new DatagramPacket(mensajeEnviar,mensajeEnviar.length,received.getAddress(),received.getPort());
				UDP.send(paqueteAEnviar);
				System.out.println("Cliente conectado: "+received.getPort()+" "+
				received.getAddress());
				
				
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}
	public static void close(){
		try {
			UDP.close();
			System.out.println("-----------LA CONEXION HA FINALIZADO------------");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static int nroPal(String cadena){
		StringTokenizer palabras=new StringTokenizer(cadena.trim());
		return palabras.countTokens();
	}
	
	
}
