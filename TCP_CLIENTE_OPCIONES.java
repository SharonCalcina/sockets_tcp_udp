package UDP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class TCP_CLIENTE_OPCIONES {
	static Socket sCliente;
	static BufferedReader entrada;
	static PrintWriter salida;
	static Scanner mensajeb;
	public static void main(String[] args) {
		try {
			//ESTABLECEMOS CONEXION CON EL SERVIDOR PARA LO QUE HAY Q DEFINIR EL PUERTO Y EL HOST
			sCliente = new Socket("localhost",8888);
			//DEFINIMOS BUFFER DE ENTRADA Y PRINTWRITER PARA LOS MENSAJES DE SALIDA
			entrada= new BufferedReader(new InputStreamReader(sCliente.getInputStream()));
			salida=new PrintWriter(new BufferedWriter(new OutputStreamWriter(sCliente.getOutputStream())),true);
			//RECIBIMOS MENSAJE DEL SERVIDOR
			mensajeb = new Scanner(sCliente.getInputStream());
			//LO IMPRIMIMOS
			System.out.println(mensajeb.nextLine());
			
		} catch (Exception e) {
			e.printStackTrace();
			close();
		}
		
		BufferedReader sc=new BufferedReader(new InputStreamReader(System.in));
		try {
			//MIENTRAS EL CLIENTE NO ESCRIBA EXIT PODRA SEGUIR REALIZANDO PETICIONES AL SERVIDOR
			boolean sw=true;
			while(sw){
					System.out.println("Elija una opcion (ejem: 1, salir)");
					System.out.println("1. Opcion 1" + "\n"+"2. Opcion 2"+ "\n"+"3. Opcion 3"+ "\n"+"salir");
					String number =sc.readLine();
					switch (number){
					case "1": 
						salida.println(number);
						System.out.println(entrada.readLine());
						break;
					case "2": 
						salida.println(number);
						System.out.println(entrada.readLine());
						break;
					case "3": 
						salida.println(number);
						System.out.println(entrada.readLine());
						break;
					case "salir": 
						salida.println(number);
						sw=false;
						break;
					}
					
				
				
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			close();
		}
		//CERRAMOS LA CONEXION CON EL SERVIDOR
		close();

		
	}
	public static void close(){
		try {
			mensajeb.close();
			salida.close();
			entrada.close();
			sCliente.close();
			System.out.println("----------conexion finalizada.........");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
