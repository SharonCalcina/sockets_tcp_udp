package UDP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class TCP_SERVER_OPCIONES {
	static ServerSocket sServidor;
	static Socket sCliente;
	static BufferedReader entrada ;
	static PrintWriter salida;
	
	public static void main(String[] args) {
		//INICIALIZAMOS EL SERVIDOR
		try {
			sServidor = new ServerSocket(8888);
			System.out.println("----- SERVIDOR TCP INICIADO ------");
			System.out.println("------ Esperando Peticiones ------");
		} catch (Exception e) {
			e.printStackTrace();
			close();
		}
		
		try {
			//MIENTRAS EL SERVIDOR ESTE CORRIENDO
			while(true){
				
				
				//ACEPTAMOS LA CONEXION
					sCliente = sServidor.accept();
					PrintStream bienvenida = new PrintStream(sCliente.getOutputStream());
					//IMPRIMIMOS MENSAJE DE BIENVENIDA
					bienvenida.println("***BIENVENIDO AL SERVIDOR ***");
					salida = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sCliente.getOutputStream())),true);
					entrada = new BufferedReader(new InputStreamReader(sCliente.getInputStream()));
					int sw=1;
					//UN WHILE PARA Q EL SERVIDOR SIGA RESPONDIENDO AL CLIENTE MIENTRAS ESTE NO ESCRIBA "EXIT"
					while(sw==1){
						//RECIBIMOS EL MENSAJE DEL CLIEINTE
						String palabra=entrada.readLine();
						//R
						System.out.println(palabra);
						//SI LA PALABRA ES EXIT SE TERMINARA LA CONEXION CON EL CLIENTE ACTUA;
						if (palabra.equals("salir")){
							System.out.println("Puerto del cliente: "+sCliente.getPort());
							System.out.println("ip del cliente:"+ sCliente.getInetAddress());
							sw=0;
						}
						else{
							if(palabra.equals("1"))
								salida.println("papel");	
							if(palabra.equals("2"))
								salida.println("piedra");
							if(palabra.equals("3"))
								salida.println("tijera");
						}
						
					
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				close();
				// TODO: handle exception
			}
			close();
			
		
	}

	//CERRAMOS LOS SOCKETS CREADOS Y DEMAS PARA FINALIZAR LA CONEXION
	public static void close(){
		try {
			salida.close();
			entrada.close();
			sServidor.close();
			sCliente.close();
			System.out.println("Conexion Finalizada...");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
